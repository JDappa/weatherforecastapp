/**
 * @author joseph dappa
 ***/

package variablesHandler;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import forecastWidgetHandlers.DaysForecast;

public abstract class AppMethods 
{
	
	
	//Gets json object item
	public static Object[] jsonArray(Object obj,String item)
	{
		JSONArray jsonArr = (JSONArray) obj;
		//Create object array with side of jsonArray
		Object[] o = new Object[jsonArr.size()];
		try
		{  
          //Iterate over json array
    		for(int i=0;i<jsonArr.size();i++)
    		{
    			//Cast json array to json object
    			JSONObject jsonObject = (JSONObject)jsonArr.get(i);
    			//Get each json object
    			o[i] = jsonObject.get(item);
    		}
		} 
		catch (Exception e){ e.printStackTrace(); }
		//Return object array
		return o; 
	}
	
	//Get each string
	public static String[] getJSONString(Object obj,String item)
	{
		//Cast Object to json array
		JSONArray jsonArr = (JSONArray) obj;
		
		String[] s = new String[jsonArr.size()];
		try
		{
            //Iterate over jsonArr array
    		for(int i=0;i<jsonArr.size();i++)
    		{
    			//Cast each jsonArr item to JSON Object
            	JSONObject jsonObj = (JSONObject)jsonArr.get(i);
            	//Convert to string
            	s[i] = jsonObj.get(item).toString();
    		}
            
		} 
		catch (Exception e) 
		{ e.printStackTrace(); } 
		
		return s;
	}
	
	
	public static JSONObject[] extractObject(Object obj)
	{
		//Cast Object to json array
		JSONArray jsonArr = (JSONArray) obj;
		
		JSONObject[] jsonObj = new JSONObject[jsonArr.size()];
		try
		{
	     //Iterate over jsonArr array
    		for(int i=0;i<jsonArr.size();i++)
    		{
    			//Cast each jsonArr item to JSON Object
            	jsonObj[i] = (JSONObject)jsonArr.get(i);
    		}
		} 
		catch (Exception e) 
		{ e.printStackTrace(); } 
		
		return jsonObj;
	}
	
	//Return object of json object
	public static Object jsonString(Object obj,String item)
	{
		JSONObject jsonFile = new JSONObject();
		
		try
		{
            jsonFile = (JSONObject) obj;
             	
		}
		catch (Exception e){ e.printStackTrace(); } 
		
		return jsonFile.get(item);
				
	}
	
	//I got the cityID from JSON file 
	public static String[] getCityAndID(String filePath, String item)
	{
		JSONParser jp = new  JSONParser();
		Object obj = new Object();
		JSONArray jsonArr = new JSONArray();
		
		try
		{
			//Read the json file
			FileReader fr = new FileReader(filePath);
			//Parse it to object
             obj = jp.parse(fr);   	
		}
		catch (Exception e) { e.printStackTrace(); } 
		
		//Parse the object to JSON array
		jsonArr = (JSONArray) obj;
		
		//Get size of the json array
		String[] s = new String[jsonArr.size()];
		try
		{
            //Iterate over jsonArr array
    		for(int i=0;i<jsonArr.size();i++)
    		{
    			//Cast each jsonArr item to JSON Object
            	JSONObject jsonObj = (JSONObject)jsonArr.get(i);
            	//Convert to string
            	s[i] = jsonObj.get(item).toString();
    		}
            
		} 
		catch (Exception e){ e.printStackTrace(); } 
		//return string array
		return s;
				
	}
	
	//Converts timestamps to Day Date e.g Monday 20
	public static String forecastDate(String strDate)
	{
		//Create hashmap, to hold unique entry
		HashMap<Integer,String>dayOfWeek = new HashMap<Integer,String>();
		
		Timestamp timestamp = Timestamp.valueOf(strDate);
		Date date = new Date(timestamp.getTime());
		
		int d =date.getDay();
		int dt = date.getDate();
		
		
		dayOfWeek.put(0, "Sunday");
		dayOfWeek.put(1, "Monday");
		dayOfWeek.put(2, "Tuesday");
		dayOfWeek.put(3, "Wednesday");
		dayOfWeek.put(4,"Thursday");
		dayOfWeek.put(5, "Friday");
		dayOfWeek.put(6, "Saturday");
		
		
		return dayOfWeek.get(d)+" "+dt;//return in this format (Monday 20)
		
	}
	
	//Converts temperature from kelvin to degree celsius
	public static String tempConversion(String temp)
	{
		//Temperature in single decimal format
		DecimalFormat df = new DecimalFormat("#.#");
		
		double kelvin = Double.valueOf(temp);
		double celsius = kelvin-273.15;
		
		//The degree sign
		String degSign = "\u00B0";
		//Return resulta
		return df.format(celsius)+degSign+"C";
	}
	
	
	//Get unique item from array of repetitive items
	public static ArrayList<String>uniqueItems(String[] items)
	{
		ArrayList<String>result = new ArrayList<String>();
		int x = 0;
		for(int i=0;i<items.length;i++)
		{
			if(!result.contains(items[i]))
			{
				result.add(x,items[i]);
				x++;
			}
		}
		return result;
	}
	
	
	
	//Convert timestamp into 12 hours
	
	public static String twelfthHourTime(String str)
	{
		Timestamp timestamp = Timestamp.valueOf(str);
		Date date = new Date(timestamp.getTime());

	    String strDateFormat = "h:mm a";
	    SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
	    
	    return  sdf.format(date);
		
	}
	
	public static String windDegToDir(String deg)
	{
		double d = Double.parseDouble(deg);
		String dir = null;
		//The degree sign
		String degSign = "\u00B0";
		
		String p ;
		
		double x = d - (int)d;
		
		if(x==0.0)
			p = ""+(int)d;
		else
			p = ""+d;
		
		
		if((d>=348.75 && d<=360)  || (d>=0 && d<=11.25))
			dir = "North";
		if(d>11.25 && d<=33.75)
			dir = "North-northeast";
		if(d>33.75 && d<=56.25)
			dir = "Northeast";
		if(d>56.25 && d<=78.75)
			dir = "East-northeast";
		if(d>78.75 && d <=101.25)
			dir = "East";
		if(d>101.25 && d <=123.75)
			dir = "East-southeast";
		if(d>123.75 && d <=146.25)
			dir = "Southeast";
		if(d>146.25 && d <=168.75)
			dir = "South-southeast";
		if(d>168.75 && d <=191.25)
			dir = "South";
		if(d>191.25 && d <=213.75)
			dir = "South-southwest";
		if(d>213.75 && d <=236.25)
			dir = "Southwest";
		if(d>236.25 && d <=258.75)
			dir = "West-Southwest";
		if(d>258.75 && d <=281.25)
			dir = "West";
		if(d>281.25 && d <=303.75)
			dir = "West-northwest";
		if(d>303.75 && d <=326.25)
			dir = "Northwest";
		if(d>326.25 && d <348.75)
			dir = "North-northwest";
			
		return dir+" ("+p+degSign+")";
		
	}
	
	
	
	

	
}

