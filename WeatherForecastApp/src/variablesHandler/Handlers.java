/**
 * @author joseph dappa
 ***/

package variablesHandler;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.simple.JSONObject;

import forecastWidgetHandlers.DailySummary;
import forecastWidgetHandlers.DaysForecast;
import forecastWidgetHandlers.MainTPH;
import forecastWidgetHandlers.Weather;
import forecastWidgetHandlers.Wind;
import sendHTTP.HTTPRequest;


public class Handlers 
{
    public static JSONObject json;
    
    public static Weather[] weather;
    public static String[] strDay,forecastTime;
    public static String[] clouds;
    public static MainTPH[] mainTPH;
    
    public static Wind[] wind;
    
    public static DaysForecast[] daysForecast,dailyWidgets;
    public static DailySummary[] dailySummary;

	public static ArrayList<String> daily;
	
	public static String currentTime;
	
	

	
	public static void createJSONObject(String cityName) throws Exception
	{
		
		json = HTTPRequest.ApiRequest(getCityID(cityName));
		
	}
	
	//Populate weather object
	public static void populateWeather()
	{
		//Get weather array size
		weather = new Weather[AppMethods.jsonArray(json.get("list"),"weather").length];
		//Initialise the iterator
		int i=0;
		for(Object o : AppMethods.jsonArray(json.get("list"),"weather"))
		{
			for(JSONObject jsonObj:AppMethods.extractObject(o))
			{
				//Create weather object
				weather[i] = new Weather();
				
				//Populate the weather object
				weather[i].setDescription((jsonObj.get("description")).toString());
				weather[i].setMain((jsonObj.get("main")).toString());
				weather[i].setIcon(( jsonObj.get("icon")).toString());
				
				i++;
			}
		}
		//Populate cloud array
		clouds = new String[AppMethods.jsonArray(json.get("list"), "clouds").length];
		
		int j=0;
		
		for(Object o : AppMethods.jsonArray(json.get("list"), "clouds"))
		{
			JSONObject jsonObj = (JSONObject)o;
			//Populate the clouds array
			clouds[j] = jsonObj.get("all").toString()+"%";
			j++;
		}
		
		//Populate wind array
		wind = new Wind[AppMethods.jsonArray(json.get("list"), "wind").length];
		int k=0;
		for(Object o : AppMethods.jsonArray(json.get("list"), "wind"))
		{
			JSONObject jsonObj = (JSONObject)o;
			
			//Create wind object for each iteration and add into the object its respective item
			
			wind[k] = new Wind();
			wind[k].setSpeed(jsonObj.get("speed").toString()+" m/s");
			wind[k].setDirection(AppMethods.windDegToDir(jsonObj.get("deg").toString()));
			
			k++;
		}
	}
	
	//Convert timestamp into (Day Date) and (hour:minutes am/pm) format
	public static void extractTimeStamp()
	{
		strDay = new String[AppMethods.jsonArray(json.get("list"),"dt_txt").length];
		forecastTime = new String[AppMethods.jsonArray(json.get("list"),"dt_txt").length];
		int i=0;
		for(Object o :AppMethods.jsonArray(json.get("list"),"dt_txt"))
		{
			forecastTime[i] = AppMethods.twelfthHourTime(o.toString());
			strDay[i]=AppMethods.forecastDate(o.toString());
			
			i++;
		}
		//The a unique (Day Date) time format
		daily = AppMethods.uniqueItems(strDay);
	}
	
	/**Populate mainTPH object with temp, pressure and humidity**/
	public static void tempPressureHumidity()
	{
		int i=0;
		mainTPH = new MainTPH[AppMethods.jsonArray(json.get("list"),"main").length];
		for(Object o :AppMethods.jsonArray(json.get("list"),"main"))
		{
			mainTPH[i] = new MainTPH();
			JSONObject obj = (JSONObject)o;
			
			//Get temperature in degree Celsius format
			mainTPH[i].setTemperature(AppMethods.tempConversion(obj.get("temp_max").toString()));
			mainTPH[i].setPressure(obj.get("pressure").toString()+" hpa");
			mainTPH[i].setHumidity(obj.get("humidity").toString()+"%");
			i++;
		}
		
	}
	
	//Get and populate all the days forecast
		public static void populateDaysForecast()
		{
			daysForecast = new DaysForecast[weather.length];
			for(int i=0;i<daysForecast.length;i++)
			{
				
				daysForecast[i] = new DaysForecast();
				
				daysForecast[i].setDay(strDay[i]);
				daysForecast[i].setTemp(mainTPH[i].getTemperature());
				daysForecast[i].setIcon(weather[i].getIcon());
				daysForecast[i].setMain(weather[i].getMain());
				daysForecast[i].setDescription(weather[i].getDescription());
				
			}
			
		}
	/*//Get local time 
	public static void localTime()
	{
		JSONObject jobj = (JSONObject)AppMethods.jsonString(json, "city");
		
		currentTime = AppMethods.getTime(jobj.get("timezone").toString());
	}
	*/
	
	

	//Count numbers of forecast in the first day only
	public static int dailyCount()
	{
		int count = 0;
		for(int i=0;i<daysForecast.length;i++)
		{
			if(strDay[0].equals(strDay[i]))
				count++;
		}
		return count;
		
	}
	//Get daily widgets and forecasts in day 1 only
	public static void getDailyWidgets()
	{
		dailyWidgets = new DaysForecast[daily.size()];
		dailySummary = new DailySummary[dailyCount()];
		
		for(int i=0;i<daily.size();i++)
        {
			int k=0;
        	for(int j=0;j<daysForecast.length;j++)
        	{
        		if(daily.get(i)==daysForecast[j].getDay())
        		{
        			dailyWidgets[i] = new DaysForecast();
        			
        			dailyWidgets[i].setDay(daysForecast[j].getDay());
        			dailyWidgets[i].setTemp(daysForecast[j].getTemp());
        			dailyWidgets[i].setIcon(daysForecast[j].getIcon());
        			dailyWidgets[i].setMain(daysForecast[j].getMain());
        			dailyWidgets[i].setDescription(daysForecast[j].getDescription());
        			
        		}
        	//Get all the forecasts in the first day only
        		if(daily.get(0).equals(daysForecast[j].getDay()))
        		{
        			dailySummary[k] = new DailySummary();
        			
        			dailySummary[k].setDay(daysForecast[j].getDay());
        			dailySummary[k].setTemp(daysForecast[j].getTemp());
        			dailySummary[k].setIcon(daysForecast[j].getIcon());
        			dailySummary[k].setMain(daysForecast[j].getMain());
        			dailySummary[k].setDescription(daysForecast[j].getDescription());
        			dailySummary[k].setTime(forecastTime[j]);
        			
        			k++;
        		}
        	}	
        }
		
	}
	
	//Get city id
	public static String getCityID(String cityName)
	{
		HashMap<String,String> cityIDMap = new HashMap<String,String>();
		int arrLength = AppMethods.getCityAndID("jsonFile/city.list.json" ,"name").length;
		for(int i=0;i<arrLength;i++)
		{
			cityIDMap.put(AppMethods.getCityAndID("jsonFile/city.list.json" ,"name")[i].toLowerCase(), AppMethods.getCityAndID("jsonFile/city.list.json" ,"id")[i]);
		}
		
		String cityID = cityIDMap.get(cityName);
		return cityID;

	}

}
