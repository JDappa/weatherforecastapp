package forecastWidgetHandlers;

public class MainTPH 
{
	//Variables instantiate
	private String temperature,pressure,humidity;
	//MainTPH constructors - Overloading
	public MainTPH(){}
	public MainTPH(String temperature,String pressure,String humidity)
	{
		setPressure(pressure);
		setHumidity(humidity);
	}
	
	/*ACCESSOR METHODS*/
	//Setters
	public void setTemperature(String temperature)
	{
		this.temperature=temperature;
	}
	public void setPressure(String pressure)
	{
		this.pressure=pressure;
	}
	public void setHumidity(String humidity)
	{
		this.humidity=humidity;
	}
	
	//Getters
	
	public String getTemperature()
	{return this.temperature;}
	public String getPressure()
	{return this.pressure;}
	public String getHumidity()
	{return this.humidity;}
	
	//Display to string
	public String toString()
	{return this.temperature+" "+this.pressure+" "+this.humidity;}
}
