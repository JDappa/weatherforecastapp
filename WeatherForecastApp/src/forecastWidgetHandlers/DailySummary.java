/**
 * @author joseph dappa
 ***/

package forecastWidgetHandlers;

public class DailySummary extends DaysForecast
{

	//Variables instantiate
	private String time;
	//DailySummary constructors - Overloading
	public DailySummary(){}
	public DailySummary(String day,String temp,String mainMessage,String description,String icon,String time)
	{
		super(day, temp, mainMessage, description, icon);
		setTime(time);
	}
	/*ACCESSOR METHODS*/
	//Setters
	
	public void setTime(String time)
	{
		this.time=time;
	}
	//Getters
	public String getTime()
	{
		return this.time;
	}
	//Display to string
	public String tostring()
	{
		return super.toString()+" "+this.time;
	}

}
