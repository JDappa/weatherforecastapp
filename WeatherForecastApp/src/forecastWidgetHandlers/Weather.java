/**
 * @author joseph dappa
 ***/

package forecastWidgetHandlers;

public class Weather 
{
	//Variables instantiate
	private String icon,mainMessage,description;

	//Weather constructors - Overloading
	public Weather() 
	{}
	public Weather(String mainMessage,String description,String icon) 
	{
		setIcon(icon);
		setMain(mainMessage);
		setDescription(description);
	}
	
	/*ACCESSOR METHODS*/
	//Setters
	
	public void setIcon(String icon)
	{
		this.icon=icon;
	}
	public void setMain(String mainMessage)
	{
		this.mainMessage=mainMessage;
	}
	public void setDescription(String description)
	{
		this.description=description;
	}
	
	//Getters

	public String getIcon()
	{
		return this.icon;
	}
	public String getMain()
	{
		return this.mainMessage;
	}
	public String getDescription()
	{
		return this.description;
	}
	
	//Display to string
	public String toString()
	{
		return this.mainMessage+" "+this.description+" "+this.icon;
	}
	

}
