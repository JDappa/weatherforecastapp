

/**
 * @author joseph dappa
 ***/

package forecastWidgetHandlers;

public class Wind 
{
	//Variables instantiate
	private String speed,direction;
	//Wind constructors - Overloading
	public Wind(){}
	public Wind(String speed,String direction) 
	{
		setSpeed(speed);
		setDirection(direction);
	}
	
	/*ACCESSOR METHODS*/
	//Setters
	
	public void setSpeed(String speed)
	{
		this.speed=speed;
	}
	public void setDirection(String direction)
	{
		this.direction=direction;
	}
	
	//Getters
	public String getSpeed()
	{return this.speed;}
	
	public String getDirection()
	{return this.direction;}
	
	//Display to string
	public String tostring()
	{
		return this.speed+ " "+this.direction;
	}

}
