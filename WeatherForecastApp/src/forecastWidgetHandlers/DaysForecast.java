/**
 * @author joseph dappa
 ***/

package forecastWidgetHandlers;



public class DaysForecast extends Weather
{
	//Variables instantiate
	private String day;
	private String temp;

	//DaysForecast constructors - Overloading
	public DaysForecast(){}
	public DaysForecast(String day,String temp,String mainMessage,String description,String icon)
	{
		super(mainMessage,description,icon);
		setDay(day);
		setTemp(temp);
		
	}
	/*ACCESSOR METHODS*/
	//Setters
	
	public void setDay(String day)
	{
		this.day=day;
	}
	
	public void setTemp(String temp)
	{
		this.temp=temp;
	}
	
	//Getters
	public String getDay()
	{
		return this.day;
	}
	
	public String getTemp()
	{
		return this.temp;
	}
	
	//Get string if needed
	public String toString()
	{
		return super.toString()+" "+this.day+" "+this.temp;
	}
	
	
}
