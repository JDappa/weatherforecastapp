/**
 * @author joseph dappa
 ***/

package sendHTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class HTTPRequest 
{

	//Request JSON Object from the api
	public static JSONObject ApiRequest(String cityID) throws Exception
	{
		
		//Create json parse object
		JSONParser parser = new JSONParser();
		//Create json object 
		JSONObject json = new JSONObject();
		
		
		//the url string
		String url = "https://api.openweathermap.org/data/2.5/forecast?id="+cityID+"&APPID=ENTER YOUR OPENWEATHERMAP API HERE";
		//Create url object from the url string
		URL obj =new URL(url);
		
		//Create connection with the obj and get the result
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		con.setRequestMethod("GET");
		
		int responseCode = con.getResponseCode();
		
		System.out.println("\nSending 'GET' request to URL: "+url);
		System.out.println("Response Code: "+responseCode);
		
		//read the result with buffered reader
		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		
		String line;
		StringBuffer response = new StringBuffer();
		//read each line and append 
		while((line=br.readLine())!=null)
		{
			response.append(line);
		}
		br.close();
		 //Cast the parse into json object
		 json = (JSONObject) parser.parse(response.toString());
		
		 //return json object
		return json;
		
	}

}
