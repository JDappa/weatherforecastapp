
/**
 * @author joseph dappa
 ***/


package mainApp;


import java.io.File;
import java.util.ArrayList;



import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import variablesHandler.Handlers;

public class WeatherForecastApp extends Application 
{
	private Stage window;
	//Initialise scenes 
	Scene initialScene,rootScene,weatherDetailScene;
	
	
	private Button btnEnter,btnBack,btnDetailBack;
	private TextField tfCityName;
	private Label lbEnter;
	private HBox hbUpper = new HBox();
	private VBox vbUpperRight = new VBox();
	private VBox vbUpperLeft = new VBox();
	private VBox vbLower = new VBox();
	
	private VBox vbWeatherDetails =  new VBox();
	VBox[] pages;
	
	
	private Image img,imgSum,btnImage,btnDetailImg;
	private ArrayList<String> forecastBackgroudColour;
	
	private ImageView iconImg,sumImg,btnImgV,btnDetailImgV;
	Text day,temperature,mainMessage,description,sumTemp,sumTime,sumDescription;
	Text vbLTemp,vbLDescription,vbLMessage,vbLCity;
	
	
	
	//Instatiate variables
	@Override
	public void init() throws Exception
	{
		
		tfCityName = new TextField();
		tfCityName.setPrefSize(200, 25);
		tfCityName.setFont(Font.font ("Optima", 20));
		
		lbEnter = new Label("Enter city from the list");
		lbEnter.setFont(Font.font ("Optima", 20));
		
		
		 
		btnEnter = new Button("Enter");
		btnEnter.setPrefSize(100, 25);
		btnEnter.setFont(Font.font ("Optima", 20));
		
		btnBack = new Button();
		btnBack.setPrefSize(100, 25);
		btnBack.setFont(Font.font ("Optima",20));
		btnBack.setStyle("-fx-background-color:lightblue;");
		
		
		
		//Add image to back button
		File btnIcon = new File("Iconlist/backBtn.png");
		btnImage = new Image( btnIcon .toURI().toString());
		btnImgV = new ImageView(btnImage);
		btnImgV.setFitWidth(40);
		btnImgV.setFitHeight(40);
		btnBack.setGraphic(btnImgV);
		
		/**Each daily widgets detail button**/
		btnDetailBack = new Button();
		btnDetailBack.setPrefSize(100, 25);
		btnDetailBack.setFont(Font.font ("Optima",20));
		btnDetailBack.setStyle("-fx-background-color:lightblue;");
		
		//Add image to back button
		File btnDetailIcon = new File("Iconlist/backBtn.png");
		btnDetailImg = new Image( btnDetailIcon.toURI().toString());
		btnDetailImgV = new ImageView(btnDetailImg);
		btnDetailImgV.setFitWidth(40);
		btnDetailImgV.setFitHeight(40);
		btnDetailBack.setGraphic(btnDetailImgV);
		
		
		//The colours for each daily widgets
		forecastBackgroudColour = new ArrayList<String>();
		
		forecastBackgroudColour.add("-fx-background-color:#008080");
		forecastBackgroudColour.add("-fx-background-color:#336600");
		forecastBackgroudColour.add("-fx-background-color:#884E4E");
		forecastBackgroudColour.add("-fx-background-color:#994C00");
		forecastBackgroudColour.add("-fx-background-color:#336600");
		forecastBackgroudColour.add("-fx-background-color:#2471A3");
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		window = primaryStage;
		//Set the application title
		window.setTitle("Weather Forecast");
		//Set the application size
		window.setHeight(600);
		window.setWidth(1000);
		
		window.setResizable(false);
		
		
		
		
		
		
		/*THE HOME PAGE*/
		//Create home page layout
		HBox initialPage = new HBox();
		GridPane gpHome = new GridPane();
		
		// - It contains the text field and enter button
		gpHome.setVgap(20);
		gpHome.setPadding(new Insets(20));
		gpHome.setAlignment(Pos.CENTER);
		
		lbEnter.setFont(Font.font(null, FontWeight.BOLD,25));
		
		
		initialPage.setAlignment(Pos.CENTER);
		initialPage.setSpacing(10);
		initialPage.getChildren().addAll(tfCityName,btnEnter);
		gpHome.add(lbEnter, 0, 0);
		gpHome.add(initialPage, 0, 1);
		gpHome.getStylesheets().add("css/root.css");
		
		initialScene = new Scene(gpHome);
		
		
		//Button to implement enter action
		btnEnter.setOnAction(ae->{
			try {
				execute();
				vbUpperRight.getChildren().add(addDailySummary()); 
				vbUpperLeft.getChildren().add(vbLeftDaily());
				
				vbLower.getChildren().add(addLowerTile());
				//Second scene
				window.setScene(rootScene);
			} catch (Exception e) {e.printStackTrace();}
		});
				
		
		
		/*THE ROOT PAGE*/
		//The Root page -It contains the daily widgets
		
		HBox root = new HBox();
		VBox vbBack = new VBox();
		VBox vbWeather= new VBox();
		
		//Go back to the previous page(Home)
		btnBack.setOnAction(ae->{
			vbUpperLeft.getChildren().clear();
			vbUpperRight.getChildren().clear();
			vbLower.getChildren().clear();
			window.setScene(initialScene);
			
			});
		
		
		vbBack.setPrefWidth(120);
		
		vbWeather.setPrefWidth(880);
		
		
		hbUpper.setPadding(new Insets(10));
		hbUpper.setPrefHeight(300);
		
		vbUpperRight.setPrefHeight(300);
		
		vbLower.setPadding(new Insets(10));
		
		vbBack.setPadding(new Insets(10));
		vbBack.getChildren().add(btnBack);
		
		
		hbUpper.getChildren().addAll(vbUpperLeft,vbUpperRight);
		//Add hbTop to root
		vbWeather.getChildren().addAll(hbUpper,vbLower);
		
		root.getChildren().addAll(vbBack,vbWeather);
		
		
		/**EACH DAILY WIDGETS DETAILS**/
		
		//Weather Detail Report
		HBox hbDetails = new HBox();
		weatherDetailScene = new Scene(hbDetails);
		VBox vbDetailsback = new VBox();
		
		vbDetailsback.setStyle("-fx-background-color:lightblue");
		vbDetailsback.getChildren().add(btnDetailBack);
		hbDetails.getChildren().addAll(vbDetailsback,vbWeatherDetails);
		
				
				btnDetailBack.setOnAction(ae->
				{
					vbWeatherDetails.getChildren().clear();
					window.setScene(rootScene);
					
				}
				);
				
				
		
		
		
		
		//Add style sheet to the root
		//root.getStylesheets().add("css/root.css");
		
		//DEFAULT SCENE - Root scene
		rootScene = new Scene(root);
		rootScene.getStylesheets().add("css/root.css");
		
		
		
		
		
		
		
		
		//Set scene
		window.setScene(initialScene);
		//show window(primary stage)
		window.show();
		
		

	}
	
	//Method to hold all files to be called after entering the contry name
	public void execute() throws Exception
	{
		String cityName = tfCityName.getText();
		if(!cityName.trim().isEmpty())
		{
			try 
			{
				cityName = cityName.toLowerCase();
				
				Handlers.createJSONObject(cityName);
				Handlers.populateWeather();
				
				Handlers.extractTimeStamp();
				Handlers.tempPressureHumidity();
				Handlers.populateDaysForecast();
				Handlers.getDailyWidgets();
				
			}catch(NullPointerException npe){}
		}
		
		
		
	}
	
	//TilePane method for daily widgets
	private TilePane addLowerTile() 
	{
		TilePane tile = new TilePane();
        tile.setVgap(4);
        tile.setHgap(4);
        
        int num = Handlers.daily.size();
        
        /**To calculate the size of each vbox**/
        /**Divide width of bottom hbox by number of unique day. The result,  
        subtract the product of each padding and the total vbox**/
        double size = (1000/num)-40;
      
        pages = new VBox[num];
        
        //For loop to generate the daily widgets
        for (int i=0; i<num; i++) 
        {
        	pages[i] = new VBox();
        	
        	iconImg = new ImageView();
        	day = new Text();
    		temperature = new Text();
    		mainMessage = new Text();
    		description = new Text();
        	//Create a file instant
    		File iconFile = new File("Iconlist/"+Handlers.dailyWidgets[i].getIcon()+".png");
    		img = new Image(iconFile.toURI().toString());
        	//Display the retrieve image on the image view
    		iconImg.imageProperty().setValue(img);
    		
    		
    		iconImg.setFitWidth(65);
    		iconImg.setFitHeight(65);
    	
    		
    		pages[i].setPrefHeight(220);
    		pages[i].setPrefWidth(size);
    		pages[i].setPadding(new Insets(5));
    		pages[i].setSpacing(10);
    		
    		
    		pages[i].setStyle(forecastBackgroudColour.get(i));
    		
    		

    		day.setFont(Font.font ("Optima", 13));
    		temperature.setFont(Font.font ("Optima", 20));
    		
    		
    		day.setText(Handlers.dailyWidgets[i].getDay());
    		temperature.setText(Handlers.dailyWidgets[i].getTemp());
    		mainMessage.setText(Handlers.dailyWidgets[i].getMain());
    		description.setText(Handlers.dailyWidgets[i].getDescription());
    		
    		
    		
        	pages[i].getChildren().addAll(day,iconImg,temperature,mainMessage,description);
        	
        	/**On mouse click on any widget trigger an event 
        	 * that shows the weather details for that day**/
        	int x = i;
        	pages[i].setOnMouseClicked(ae->
        	{
        		//Array list to hold the details for that day
        		ArrayList<String>time = new ArrayList<String>();
        		ArrayList<String>icon = new ArrayList<String>();
        		ArrayList<String>temp = new ArrayList<String>();
        		ArrayList<String>description = new ArrayList<String>();
        		ArrayList<String>windSpeed = new ArrayList<String>();
        		ArrayList<String>windDirection = new ArrayList<String>();
        		ArrayList<String>clouds = new ArrayList<String>();
        		ArrayList<String>humidity = new ArrayList<String>();
        		ArrayList<String>pressure = new ArrayList<String>();
        		
        		
        		int numDetails = 0;
        		for(int j=0;j<Handlers.daysForecast.length;j++)
        		{
        			if(Handlers.daily.get(x).equals(Handlers.daysForecast[j].getDay()))
        			{
        				numDetails++;
        				//GET THE ITEMS THAT CORRESPOND TO THE SELECTED DAILY WIDGETS
        				time.add(Handlers.forecastTime[j]);
        				icon.add(Handlers.daysForecast[j].getIcon());
        				temp.add(Handlers.mainTPH[j].getTemperature());
        				description.add(Handlers.daysForecast[j].getDescription());
        				windSpeed.add(Handlers.wind[j].getSpeed());
        				windDirection.add(Handlers.wind[j].getDirection());
        				clouds.add(Handlers.clouds[j]);
        				humidity.add(Handlers.mainTPH[j].getHumidity());
        				pressure.add(Handlers.mainTPH[j].getPressure());
        			}
        		}
        		
        		/**DAY**/
        		HBox hbDetailsDay = new HBox();
        		Text detailsDay = new Text();
        		detailsDay.setText(Handlers.dailyWidgets[x].getDay());
        		detailsDay.setFont(Font.font ("Optima", 20));
        		
        		hbDetailsDay.setPadding(new Insets(5,10,5,10));
        		hbDetailsDay.setStyle("-fx-background-color:lightgray");
        		hbDetailsDay.getChildren().add(detailsDay);
        		
        		VBox vbGPDetailsContainer = new VBox();
        		GridPane[] gpDetails = new GridPane[numDetails];
        		
        		
        		for(int j=0;j<Handlers.daysForecast.length;j++)
        		{
        			if(Handlers.daily.get(x)==Handlers.daysForecast[j].getDay())
        			{
        				
        				for(int k=0;k<gpDetails.length;k++)
        				{
        					//LAYOUTS
        					HBox hbTimeIcon = new HBox();
        					VBox vbOtherItems = new VBox();
        					HBox hbTempDescribe = new HBox();
        					HBox hbWind = new HBox();
        					
        					gpDetails[k] = new GridPane();
            				
            				/**TIME**/
                    		Text detailsTime = new Text();
                    		detailsTime.setText(time.get(k));
                    		detailsTime.setFont(Font.font ("Optima", 12));
                    		
                    		/**ICON**/
                    		ImageView detailsIcon = new ImageView();
                    		//Create a file instant and get the icon
                    		File detailIconFile = new File("Iconlist/"+icon.get(k)+".png");
                    		Image detailsImg = new Image(detailIconFile.toURI().toString());
                        	//Display the retrieve image on the image view
                    		detailsIcon.imageProperty().setValue(detailsImg);
                    		
                    		detailsIcon.setFitWidth(50);
                    		detailsIcon.setFitHeight(50);
                    		
                    		
                    		//hbTimeIcon.setSpacing(5);
                    		hbTimeIcon.setPadding(new Insets(5,0,5,0));
                    		hbTimeIcon.setAlignment(Pos.CENTER_LEFT);
                    		
                    		hbTimeIcon.getChildren().addAll(detailsTime,detailsIcon);
                    		
                    		/**TEMPERATURE**/
                    		Text temperature = new Text();
                    		temperature.setText(temp.get(k));
                    		temperature.setFont(Font.font ("Optima", 12));
                    		
                    		/**DESCRIPTION MESSAGE**/
                    		Text describe = new Text();
                    		describe.setText(description.get(k));
                    		describe.setFont(Font.font ("Optima", 12));
                    		
                    		hbTempDescribe.setSpacing(10);
                    		//Add children
                    		hbTempDescribe.getChildren().addAll(temperature,describe);
                    		
                    		/**WIND SPEED**/
                    		Text wSpeed = new Text();
                    		wSpeed.setText(windSpeed.get(k)+",");
                    		wSpeed.setFont(Font.font ("Optima", 12));
                    		
                    		/**WIND DIRECTION**/
                    		Text wDirection = new Text();
                    		wDirection.setText(windDirection.get(k)+",");
                    		wDirection.setFont(Font.font ("Optima", 12));
                    		
                    		/**CLOUDS**/
                    		Text lbClouds = new Text("Clouds:");
                    		Text txtClouds = new Text();
                    		txtClouds.setText(clouds.get(k)+",");
                    		txtClouds.setFont(Font.font ("Optima", 12));
                    		
                    		/**HUMIDITY**/
                    		Text lbHumidity = new Text("Humidity:");
                    		Text txtHumidity = new Text();
                    		txtHumidity.setText(humidity.get(k)+",");
                    		txtHumidity.setFont(Font.font ("Optima", 12));
                    		
                    		/**PRESSURE**/
                    		Text lbPressure = new Text("Pressure:");
                    		Text txtPressure = new Text();
                    		txtPressure.setText(pressure.get(k));
                    		txtPressure.setFont(Font.font ("Optima", 12));
                    	
                    		
                    		hbWind.setSpacing(5);
                    		//Add the components into the hbox
                    		hbWind.getChildren().addAll(wSpeed,wDirection,lbClouds,txtClouds,lbHumidity,txtHumidity,lbPressure,txtPressure);
                    		
                    		
                    		vbOtherItems.setSpacing(10);
                    		vbOtherItems.setAlignment(Pos.CENTER_RIGHT);
                    		vbOtherItems.getChildren().addAll(hbTempDescribe,hbWind);
                    		
                    		
                    		gpDetails[k].setVgap(5);
                    		gpDetails[k].setHgap(170);
                    		gpDetails[k].setPadding(new Insets(3,27,3,27));
                    		
                    		
                    		
                    		
                    		gpDetails[k].add(hbTimeIcon, 0, 0);
                    		gpDetails[k].add(vbOtherItems, 1, 0);
                    		
                    		vbGPDetailsContainer.getChildren().add(gpDetails[k]);
        				}//End of inner for loop
        			}//End of if statement
        			
        		}//End of outer for loop
        		
        		vbWeatherDetails.setSpacing(10);
        		vbWeatherDetails.setPadding(new Insets(10));
        		vbWeatherDetails.getChildren().addAll(hbDetailsDay,vbGPDetailsContainer);
        		
        		weatherDetailScene.getStylesheets().add("css/root.css");
        		window.setScene(weatherDetailScene);
        		//System.out.println(x);
        	});
        	
            tile.getChildren().add(pages[i]);
        }

        return tile;
	}
	
	//TilePane for the day summary
	
	public TilePane addDailySummary()
	{
		
		TilePane tile = new TilePane();
		
		
		tile.setVgap(4);
        tile.setHgap(4);
        
        tile.setPadding(new Insets(15,0,0,0));
        
       int num = Handlers.dailySummary.length-2;
		
		VBox[] vbSummary = new VBox[num];
		
        for(int i=1;i<num;i++)
        {
        	vbSummary[i]= new VBox();
        	
        	sumImg = new ImageView();
        	
        	
    		sumTemp = new Text();
    		sumTime = new Text();
    		sumDescription = new Text();
    		
    		File icon = new File("Iconlist/"+Handlers.dailySummary[i].getIcon()+".png");
    		imgSum = new Image(icon.toURI().toString());
    		//Display the retrieve image on the image view
    		sumImg.imageProperty().setValue(imgSum);
    		
    		
    		sumImg.prefHeight(20);
    		sumTemp.setText(Handlers.dailySummary[i].getTemp());
    		sumTime.setText(Handlers.dailySummary[i].getTime());
    		sumDescription.setText(Handlers.dailySummary[i].getDescription());
    		
    		vbSummary[i].getChildren().addAll(sumTemp,sumImg,sumTime,sumDescription);
    		vbSummary[i].setSpacing(5);
    		vbSummary[i].setPadding(new Insets(10,2.5,0,2.5));
    		vbSummary[i].setAlignment(Pos.BOTTOM_RIGHT);
    		
    		tile.getChildren().add(vbSummary[i]);
        }
       
		return tile;
	}
	
	//VBox method to hold the city,temperature etc
	private VBox vbLeftDaily()
	{
		VBox vbLeft = new VBox();
		
		VBox vbTemp = new VBox();
		
		VBox vbCity = new VBox();
		
		VBox vbExtra = new VBox();
		
		
		GridPane gpTempExtra = new GridPane();
		
		
		vbLeft.prefWidth(120);
		
		
		vbLTemp = new Text();
		vbLMessage = new Text();
		vbLDescription = new Text();
		vbLCity = new Text();
		
		
		vbLCity.setText(tfCityName.getText());
		
		
		
		vbLTemp.setText(Handlers.dailySummary[0].getTemp());
		vbLMessage.setText(Handlers.dailySummary[0].getMain());
		vbLDescription.setText(Handlers.dailySummary[0].getDescription());
		
		vbLCity.setFont(Font.font ("Optima", 30));
		vbLTemp.setFont(Font.font ("Optima", 30));
		
		vbLeft.setSpacing(5);
		vbLeft.setAlignment(Pos.BOTTOM_LEFT);
		
		vbTemp.getChildren().add(vbLTemp);
		vbTemp.setPadding(new Insets(5));
		vbTemp.setAlignment(Pos.CENTER);
		
		vbCity.setPadding(new Insets(5));
		vbCity.setAlignment(Pos.BOTTOM_LEFT);
		vbCity.getChildren().add(vbLCity);
		
		gpTempExtra.setHgap(5);
		gpTempExtra.setVgap(10);
		gpTempExtra.setPadding(new Insets(25,0,0,0));
		gpTempExtra.add(vbTemp, 0, 5);
		gpTempExtra.add(vbExtra, 1, 6);
		
		vbExtra.getChildren().addAll(vbLMessage,vbLDescription);
		vbExtra.setSpacing(5);
		vbExtra.setPadding(new Insets(10));
		vbExtra.setAlignment(Pos.CENTER_LEFT);
		
	
		
		vbLeft.getChildren().addAll(gpTempExtra,vbCity);
		
		return vbLeft;
	}


	public static void main(String[] args) 
	{
		//Launch application
		launch(args);
	}

}

