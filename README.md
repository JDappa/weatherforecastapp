
NB:


1. First get your api key from https://openweathermap.org


2. When you get your key, go to "HTTPRequest.java", you will see where to add your key.


3. Most of the city names on the list do not have the actual English alphabets. I will advice you to copy the city from the text file and paste it into the text field. Typing it won't work for few cities. Example, "istanbul" won't if you type on keyboard, but copying and pasting do work.
